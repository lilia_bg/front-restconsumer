import { LitElement, html, css } from 'lit-element';

class TestOpentable  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
      td {
          text-align: center;
      }
      button {
          float: left;
      }
    `;
  }

  static get properties() {
    return {
        resultados: {type:Object},
        campos: {type: Array},
        filas: {type:Array},
        sig: {type: Boolean},
        prev: {type: Boolean},
        entidades: {type: Array}
    };
  }

  constructor() {
    super();
    this.campos = [];
    this.resultados = {results : []};
    this.filas = [];
    this.cargarDatos("https://swapi.dev/api/planets/");
    this.entidades = ["planets","films","people","species","starship","vehicles"];
  }

  render() {
    return html`
      <div>
        <select id="sel" @change="${this.changeSel}">
          ${this.entidades.map(e => html`<option>${e}</option>`)}
        </select>
      </div>
      <table width = "100%">
        <tr>
            ${this.campos.map(c => html`<th>${c}</th>`)}
        </tr>
        ${this.filas.map(i => html `<tr>
            ${this.resultados.results[i].valores.map(v => html `<td>${v}</td>`)}
        </tr>`)}
      </table>
      ${this.prev? html `
      <div><button @click="${this.anterior}">ANTERIOR</button></div>`: html ``}
        ${this.sig? html `
        <div><button @click="${this.siguiente}">SIGUIENTE</button></div>`: html ``}
    `;
  }
  changeSel(){
    var entidad = this.shadowRoot.querySelector("#sel").value;
    var url = "https://swapi.dev/api/" + entidad + "/";
    this.cargarDatos(url);
  }
  anterior(){
      this.cargarDatos(this.resultados.previous);
  }
  siguiente(){
      this.cargarDatos(this.resultados.next);
  }

  cargarDatos(url){
      fetch(url)
        .then(response => {
            console.log(response);
            if (!response.ok){ throw response;} //exception lanza response ya que no esta ok
            return response.json(); //lo devuelve para el siguiente then, si no es error llama al siguiente then
        })
        .then(data => {
            this.resultados = data;
            this.montarResultados();
            console.log(data);
        })
        .catch(error => {
            alert("Error en fetch: " + error);
        })
  }

  montarResultados(){
      this.campos = this.getObjectProps(this.resultados.results[0]);
      this.filas = [];
      for (var i=0; i < this.resultados.results.length; i++){
          this.resultados.results[i].valores = this.getValores(this.resultados.results[i]);
          this.filas.push(i);
      }
      this.sig = (this.resultados.next != null);
      this.prev = (this.resultados.previous != null);
  }
  getValores(fila){
      var valores = [];
      for (var i = 0; i < this.campos.length; i++){
          valores[i] = fila[this.campos[i]];
      }
      return valores;
  }
  getObjectProps(obj){
      var props = [];
      for ( var prop in obj){
          if(!this.isArray(this.resultados.results[0][prop])){
              if(prop !== "created" && prop !== "edited" && prop !== "url"){
                  props.push(prop);
              }
          }
      }
      return props;
    }
  isArray(prop){
      return Object.prototype.toString.call(prop) === '[object Array]';
  }



}

customElements.define('test-opentable', TestOpentable);