import { LitElement, html, css } from 'lit-element';

class TestFetch  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        planets: {type:Object}
    };
  }

  constructor() {
    super();
    this.planets = {results :[]};
  }

  render() {
    return html`
      ${this.planets.results.map((pl) => {
          return html `<div>${pl.name} ${pl.rotation_period}</div>`
      })}
    `;
  }

  //cuando termina de cargar todo lo necesario. Inicializamos despues de que se renderice 
  connectedCallback(){ //se ejecuta despues de que termina la carga en el componente que lo va a tener //fires each time a custom element is appended into a document-
      super.connectedCallback();
      try {
          this.cargarPlanetas();
      } catch (e) {
          alert(e);
      }
  }

  cargarPlanetas(){
      fetch("https://swapi.dev/api/planets/")//si no se cambia el fetch es de tipo GET 
        .then(response => {
            console.log(response);
            if (!response.ok){ throw response;} //exception lanza response ya que no esta ok
            return response.json(); //lo devuelve para el siguiente then, si no es error llama al siguiente then
        })
        .then(data => {
            this.planets = data;
            console.log(data);
        })
        .catch(error => {
            alert("Error en fetch: " + error);
        })
  }
}

customElements.define('test-fetch', TestFetch);