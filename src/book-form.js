import { LitElement, html, css } from 'lit-element';

class BookForm  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        id:{type: Number},
        titulo: {type: String},
        autor: {type: String}
    };
  }

  constructor() {
    super();
    this.id = 0;
    this.titulo = "";
    this.autor = "";
  }
  //.value nos garantizza que se refresca en la UI y lo que pongamos en lavariable se refresque en pantalla

  render() {
    return html`
      <div>
        <label for="iid">ID</label>
        <input type="number" id="iid" .value="${this.id}" @input="${this.updateId}" />
        <br/>
        <label for="ititulo">TITULO</label>
        <input type="text" id="ititulo" .value="${this.titulo}" @input="${this.updateTitulo}" />
        <br/>
        <label for="iautor">Autor</label>
        <input type="text" id="iautor" .value="${this.autor}" @input="${this.updateAutor}" />
        <br/>
        <br/>
        <button @click = "${this.buscarBooks}">Buscar</button>
        <button @click = "${this.crearBook}">Crear</button>
        <button @click = "${this.modificarBook}">Modificar</button>
        <button @click = "${this.eliminarBook}">Eliminar</button>
      </div>
    `;
  }

    getCurrentBook(){
        var book = {};
        book.id = this.id;
        book.titulo = this.titulo;
        book.autor = this.autor;
        return book;
    }
    eliminarBook(){
      var book= this.getCurrentBook();
      const options = {
          method : 'DELETE',
          body : "",
          headers: {'Content-type' : "application/json"} 
      };
      fetch("http://localhost:3392/books/" + this.id, options)
      .then(response => {
          console.log(response);
          if (!response.ok){ throw response;} //exception lanza response ya que no esta ok
          return response.json(); //lo devuelve para el siguiente then, si no es error llama al siguiente then
      })
      .then(data => {
          alert("Eliminado");
      })
      .catch(error => {
          alert("Error en fetch: " + error);
      })
    }
    crearBook(){
        var book= this.getCurrentBook();
        const options = {
            method : 'POST',
            body : JSON.stringify(book),
            headers: {'Content-type' : "application/json"} 
        };
        fetch("http://localhost:3392/books", options)
            .then(response => {
                console.log(response);
                if (!response.ok){ throw response;} //exception lanza response ya que no esta ok
                return response.json(); //lo devuelve para el siguiente then, si no es error llama al siguiente then
            })
            .then(data => {
                alert("book creado");
            })
            .catch(error => {
                alert("Error en fetch: " + error);
            })
    }

    modificarBook(){
      var book= this.getCurrentBook();
      const options = {
          method : 'PUT',
          body : JSON.stringify(book),
          headers: {'Content-type' : "application/json"} 
      };
      fetch("http://localhost:3392/books", options)
          .then(response => {
              console.log(response);
              if (!response.ok){ throw response;} //exception lanza response ya que no esta ok
              return response.json(); //lo devuelve para el siguiente then, si no es error llama al siguiente then
          })
          .then(data => {
              alert("book modificado");
          })
          .catch(error => {
              alert("Error en fetch: " + error);
          })
  }

    buscarBooks(){
        fetch("http://localhost:3392/books/" + this.id)
            .then(response => {
                console.log(response);
                if (!response.ok){ throw response;} //exception lanza response ya que no esta ok
                return response.json(); //lo devuelve para el siguiente then, si no es error llama al siguiente then
            })
            .then(data => {
                this.id = data.id;
                this.titulo = data.titulo;
                this.autor = data.autor;               
                console.log(data);
            })
            .catch(error => {
                alert("Error en fetch: " + error);
            })
    }

  updateId(e){
    this.id = parseInt(e.target.value);

  }
  updateTitulo(e){
    this.titulo = e.target.value;
  }

  updateAutor(e){
    this.autor = e.target.value;
  }


}

customElements.define('book-form', BookForm);