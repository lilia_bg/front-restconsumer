import { LitElement, html, css } from 'lit-element';

class ProductsList  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        productos:{type:Array}
    };
  }

  constructor() {
    super();
    this.productos = [];
    this.productos.push({"nombre":"Movil XL","descripcion":"Un telefono grande con una de las mejores pantallas"});
    this.productos.push({"nombre":"Movil Mini","descripcion":"Un telefono mediano con una de las mejores camaras"});
    this.productos.push({"nombre":"Movil Standard","descripcion":"Un telefono standard. Nada especial"});
  }

  render() {
    return html`
      <div class="contenedor">
        ${this.productos.map(p => html`<div class="producto"><h3>${p.nombre}</h3><p>${p.descripcion}</p></div>`)}
      </div>
    `;
  }
  //no haya shadow, el shadow es para evitar que el codigo externo afecte al codigo interno pero como lo vamos a probar entonces lo hacemos
  //para que todo el shadow este disponible 
  createRenderRoot(){
      return this;
  }
}

customElements.define('products-list', ProductsList);