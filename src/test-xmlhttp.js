import { LitElement, html, css } from 'lit-element';

class TestXmlhttp  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        planet: {type: Object}
    };
  }

  constructor() {
    super();
    this.cargarPlaneta();
  }

  render() {
    return html`
      <p><code>${this.planet}</code></p>
    `;
  }


//servicio web -  peticion que puede tardar o que no responda
    cargarPlaneta(){
        //xmlHttpRequest
        var req = new XMLHttpRequest();
        req.open('GET', 'http://swapi.dev/api/planets/1', true);
        req.onreadystatechange =
            ((aEvt) => {
                if(req.readyState === 4){ //respuesta de llamadas que se envie en varios paquetes de REST, empezar a procesar la info hasta que haya recibido todo, 4 ya termino de recibir los paquetes 
                    if(req.status === 200){
                        this.planet = req.responseText;
                        this.requestUpdate();
                    } else {
                        alert("Error llamando rest");
                    }
                }
            })
        ;
        req.send(null); //send pide el cuerpo de la solicitud, no necesitamos mandar nada (POST,PUT)
    }

}
customElements.define('test-xmlhttp', TestXmlhttp);