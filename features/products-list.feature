Features: Lista Productos
    Scenario: Cargar lista de productos
        When we request the Products List
        Then we should receive
            | nombre | descripcion | 
            | Movil XL | Un telefono grande con una de las mejores pantallas | 
            | Movil Mini | Un telefono mediano con una de las mejores camaras |  
            | Movil Standard |      |  