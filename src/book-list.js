import { LitElement, html, css } from 'lit-element';

class BookList  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        data: {type:Object}
    };
  }

  constructor() {
    super();
    this.data = {"books": []};
    this.cargarDatos();
  }


  render() {
    return html`
      <div>
        ${this.data.books.map((b) => html `<div>${b.titulo} - ${b.autor}</div>`) }
      </div>
    `;
  }

  cargarDatos(){
    fetch("http://localhost:3392/books")
      .then(response => {
          console.log(response);
          if (!response.ok){ throw response;} //exception lanza response ya que no esta ok
          return response.json(); //lo devuelve para el siguiente then, si no es error llama al siguiente then
      })
      .then(data => {
          this.data = data;
          console.log(data);
      })
      .catch(error => {
          alert("Error en fetch: " + error);
      })
}

}

customElements.define('book-list', BookList);